import DateClass.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Date date1 = new Date();
        Date date2 = new Date(31, 12, 2022);

        date1.setDate(9, 6, 1995);
        System.out.println("Date 1");
        System.out.println(date1.toString());
        System.out.println("Date 2");
        System.out.println(date2.toString());

    }
}
